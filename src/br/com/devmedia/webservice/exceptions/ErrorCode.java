package br.com.devmedia.webservice.exceptions;

/**
 * Enum que contem os dados das mensagens de erros personalizadas.
 * 
 * @category BAD_REQUEST - Status 400 - ops, h� algo errado nesta p�gina ou
 *           configura��es do servidor
 * @category NOT_FOUND - Status 404 - O recurso requisitado n�o foi encontrado,
 *           mas pode ser disponibilizado novamente no futuro. As solicita��es
 *           subsequentes pelo cliente s�o permitidas
 * @category SERVER_ERROR - Status 500; dica um erro do servidor ao processar a
 *           solicita��o. Na grande maioria dos casos est� relacionada as
 *           permiss�es dos arquivos ou pastas do software ou script que o
 *           usu�rio tenta acessar e n�o foram configuradas no momento da
 *           programa��o/constru��o do site ou da aplica��o. Para corrigir,
 *           verifique o diret�rio em que o arquivo ou recurso que houve falha
 *           de acesso est� localizado, e este arquivo (bem como todos os
 *           outros)
 */
public enum ErrorCode {

	BAD_REQUEST(400), NOT_FOUND(404), SERVER_ERROR(500);

	private int code;

	ErrorCode(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

}
