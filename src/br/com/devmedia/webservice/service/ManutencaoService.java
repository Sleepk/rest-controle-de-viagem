package br.com.devmedia.webservice.service;

import java.util.List;

import br.com.devmedia.webservice.model.dao.ManutencaoDAO;
import br.com.devmedia.webservice.model.domain.Manutencao;

public class ManutencaoService {
	private ManutencaoDAO dao = new ManutencaoDAO();

	public List<Manutencao> getManutencoes(long id) {
		return dao.getAll(id);
	}

	public Manutencao getManutencao(Long id) {
		return dao.getById(id);
	}

	public Manutencao saveManutencao(long id, Manutencao produto) {
		return dao.save(id, produto);
	}

	public Manutencao updateManutencao(long id, Manutencao produto) {
		return dao.update(id, produto);
	}

	public Manutencao deleteManutencao(Long id) {
		return dao.delete(id);
	}

	public List<Manutencao> getManutencaosByPagination(long id, int firstResult, int maxResults) {
		return dao.getByPagination(id, firstResult, maxResults);
	}

}
