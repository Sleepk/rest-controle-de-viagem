package br.com.devmedia.webservice.service;

import java.util.List;

import br.com.devmedia.webservice.model.dao.MotoristaDAO;
import br.com.devmedia.webservice.model.domain.Motorista;

public class MotoristaService {

	private MotoristaDAO dao = new MotoristaDAO();
	
	public List<Motorista> getMotoristas() {
		return dao.getAll();
	}
	
	public Motorista getMotorista(Long id) {
		return dao.getById(id);
	}
	
	public Motorista saveMotorista(Motorista produto) {
		return dao.save(produto);
	}
	
	public Motorista updateMotorista(Motorista produto) {
		return dao.update(produto);
	}
	
	public Motorista deleteMotorista(Long id) {
		return dao.delete(id);
	}
	
	public List<Motorista> getMotoristasByPagination(int firstResult, int maxResults) {
		return dao.getByPagination(firstResult, maxResults);
	}
	
	public List<Motorista> getMotoristaByName(String name) {
		return dao.getByName(name);
	}
	
}
