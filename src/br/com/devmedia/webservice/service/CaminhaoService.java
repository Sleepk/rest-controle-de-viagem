package br.com.devmedia.webservice.service;

import java.util.List;

import br.com.devmedia.webservice.model.dao.CaminhaoDAO;
import br.com.devmedia.webservice.model.domain.Caminhao;

public class CaminhaoService {

	private CaminhaoDAO dao = new CaminhaoDAO();
	
	public List<Caminhao> getCaminhoes() {
		return dao.getAll();
	}
	
	public Caminhao getCaminhao(Long id) {
		return dao.getById(id);
	}
	
	public Caminhao saveCaminhao(Caminhao caminhao) {
		return dao.save(caminhao);
	}
	
	public Caminhao updateCaminhao(Caminhao caminhao) {
		return dao.update(caminhao);
	}
	
	public Caminhao deleteCaminhao(Long id) {
		return dao.delete(id);
	}
	
	public List<Caminhao> getCaminhaosByPagination(int firstResult, int maxResults) {
		return dao.getByPagination(firstResult, maxResults);
	}
	
	public List<Caminhao> getCaminhaoByName(String placa) {
		return dao.getByName(placa);
	}
	
}
