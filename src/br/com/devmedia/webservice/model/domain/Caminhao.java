package br.com.devmedia.webservice.model.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@XmlRootElement
public class Caminhao implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1733957398027052591L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idcaminhao")
	private Long id;

	@Column(name = "cam_placa")
	private String placa;

	@Column(name = "cam_renavam", unique = true)
	private String renavam;

	@Column(name = "cam_anofabricacao")
	private LocalDate anoFabricacao;

	@Column(name = "cam_modelo")
	private String modelo;

	@Column(name = "cam_marca")
	private String marca;

	@Column(name = "cam_odometro")
	private Double odometro;

	@Column(name = "cam_consumomediolkm")
	private BigDecimal consumoMedioKM;

	@Column(name = "cam_status")
	private String status;

	@Column(name = "cam_chassi")
	private String cassi;

	@OneToMany(mappedBy = "caminhao", fetch = FetchType.EAGER)
	@JsonIgnore
	private List<Manutencao> manutencao = new ArrayList<>();

	public List<Manutencao> getManutencao() {
		return manutencao;
	}

	public void setManutencao(List<Manutencao> manutencao) {
		this.manutencao = manutencao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getRenavam() {
		return renavam;
	}

	public void setRenavam(String renavam) {
		this.renavam = renavam;
	}

	public LocalDate getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(LocalDate anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public Double getOdometro() {
		return odometro;
	}

	public void setOdometro(Double odometro) {
		this.odometro = odometro;
	}

	public BigDecimal getConsumoMedioKM() {
		return consumoMedioKM;
	}

	public void setConsumoMedioKM(BigDecimal consumoMedioKM) {
		this.consumoMedioKM = consumoMedioKM;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCassi() {
		return cassi;
	}

	public void setCassi(String cassi) {
		this.cassi = cassi;
	}

}