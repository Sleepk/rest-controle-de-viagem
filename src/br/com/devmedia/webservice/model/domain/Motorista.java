package br.com.devmedia.webservice.model.domain;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Classe modelo do motorista contendo as informacoes basica. 
 * 
 * Como, mot_nome, mot_cpf, mot_rg, mot_orgaoExpedidor, mot_dataNasc, mot_cnh
 * mot_sexo, mot_telefone, mot_celular, mot_status
 */
@Entity
@XmlRootElement
public class Motorista implements Serializable {

	
	private static final long serialVersionUID = 8840276767518953702L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idmotorista")
	private Long id;


	@Column(name = "mot_celular", length = 13)
	private String celular;

	@Column(name = "mot_cnh")
	private String cnh;

	@Column(name = "mot_cpf", unique = true, length = 13)
	private String cpf;

	@Column(name = "mot_datanasc")
	private LocalDate dataDeNascimento;

	@Column(name = "mot_nome", length = 45)
	private String nome;

	@Column(name = "mot_orgaoexpedidor")
	private String orgaoExpedidor;

	@Column(name = "mot_rg")
	private String rg;

	@Column(name = "mot_sexo")
	private char sexo;

	@Column(name = "mot_status", length = 15)
	private String status;

	@Column(name = "mot_telefone", nullable = false)
	private String telefone;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCnh() {
		return cnh;
	}

	public void setCnh(String cnh) {
		this.cnh = cnh;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public LocalDate getDataDeNascimento() {
		return dataDeNascimento;
	}

	public void setDataDeNascimento(LocalDate dataDeNascimento) {
		this.dataDeNascimento = dataDeNascimento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getOrgaoExpedidor() {
		return orgaoExpedidor;
	}

	public void setOrgaoExpedidor(String orgaoExpedidor) {
		this.orgaoExpedidor = orgaoExpedidor;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Motorista [id=");
		builder.append(id);
		builder.append(", celular=");
		builder.append(celular);
		builder.append(", cnh=");
		builder.append(cnh);
		builder.append(", cpf=");
		builder.append(cpf);
		builder.append(", dataDeNascimento=");
		builder.append(LocalDate.now());
		builder.append(", nome=");
		builder.append(nome);
		builder.append(", orgaoExpedidor=");
		builder.append(orgaoExpedidor);
		builder.append(", rg=");
		builder.append(rg);
		builder.append(", sexo=");
		builder.append(sexo);
		builder.append(", status=");
		builder.append(status);
		builder.append(", telefone=");
		builder.append(telefone);
		builder.append("]");
		return builder.toString();
	}

}
