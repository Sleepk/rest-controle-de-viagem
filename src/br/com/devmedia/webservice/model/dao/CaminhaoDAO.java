package br.com.devmedia.webservice.model.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.devmedia.webservice.exceptions.DAOException;
import br.com.devmedia.webservice.exceptions.ErrorCode;
import br.com.devmedia.webservice.model.domain.Caminhao;

public class CaminhaoDAO {

	public List<Caminhao> getAll() {
		EntityManager em = JPAUtil.getEntityManager();
		List<Caminhao> caminhaos = null;

		try {
			caminhaos = em.createQuery("select p from Caminhao p", Caminhao.class).getResultList();
		} catch (RuntimeException ex) {
			throw new DAOException("Erro ao recuperar todos os Caminhao do banco: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}

		return caminhaos;
	}

	public Caminhao getById(long id) {
		EntityManager em = JPAUtil.getEntityManager();
		Caminhao caminhao = null;

		if (id <= 0) {
			throw new DAOException("O id precisa ser maior do que 0.", ErrorCode.BAD_REQUEST.getCode());
		}

		try {
			caminhao = em.find(Caminhao.class, id);
		} catch (RuntimeException ex) {
			throw new DAOException("Erro ao buscar Caminhao por id no banco de dados: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}

		if (caminhao == null) {
			throw new DAOException("Caminhao de id " + id + " n�o existe.", ErrorCode.NOT_FOUND.getCode());
		}

		return caminhao;
	}

	public Caminhao save(Caminhao caminhao) {
		EntityManager em = JPAUtil.getEntityManager();

		if (!CaminhaoIsValid(caminhao)) {
			throw new DAOException("Caminhao com dados incompletos.", ErrorCode.BAD_REQUEST.getCode());
		}

		try {
			em.getTransaction().begin();
			em.persist(caminhao);
			em.getTransaction().commit();
		} catch (RuntimeException ex) {
			em.getTransaction().rollback();
			throw new DAOException("Erro ao salvar Caminhao no banco de dados: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}
		return caminhao;
	}

	public Caminhao update(Caminhao caminhao) {
		EntityManager em = JPAUtil.getEntityManager();
		Caminhao caminhaoManaged = null;

		if (caminhao.getId() <= 0) {
			throw new DAOException("O id precisa ser maior do que 0.", ErrorCode.BAD_REQUEST.getCode());
		}
		if (!CaminhaoIsValid(caminhao)) {
			throw new DAOException("Caminhao com dados incompletos.", ErrorCode.BAD_REQUEST.getCode());
		}

		try {
			em.getTransaction().begin();
			caminhaoManaged = em.find(Caminhao.class, caminhao.getId());
			caminhaoManaged.setAnoFabricacao(caminhao.getAnoFabricacao());
			caminhaoManaged.setConsumoMedioKM(caminhao.getConsumoMedioKM());
			caminhaoManaged.setMarca(caminhao.getMarca());
			caminhaoManaged.setModelo(caminhao.getMarca());
			caminhaoManaged.setOdometro(caminhao.getOdometro());
			caminhaoManaged.setPlaca(caminhao.getPlaca());
			caminhaoManaged.setRenavam(caminhao.getRenavam());
			caminhaoManaged.setStatus(caminhao.getStatus());
			em.getTransaction().commit();
		} catch (NullPointerException ex) {
			em.getTransaction().rollback();
			throw new DAOException("Caminhao informado para atualiza��o n�o existe: " + ex.getMessage(),
					ErrorCode.NOT_FOUND.getCode());
		} catch (RuntimeException ex) {
			em.getTransaction().rollback();
			throw new DAOException("Erro ao atualizar Caminhao no banco de dados: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}
		return caminhaoManaged;
	}

	public Caminhao delete(Long id) {
		EntityManager em = JPAUtil.getEntityManager();
		Caminhao caminhao = null;

		if (id <= 0) {
			throw new DAOException("O id precisa ser maior do que 0.", ErrorCode.BAD_REQUEST.getCode());
		}

		try {
			em.getTransaction().begin();
			caminhao = em.find(Caminhao.class, id);
			em.remove(caminhao);
			em.getTransaction().commit();
		} catch (IllegalArgumentException ex) {
			em.getTransaction().rollback();
			throw new DAOException("Caminhao informado para remo��o n�o existe: " + ex.getMessage(),
					ErrorCode.NOT_FOUND.getCode());
		} catch (RuntimeException ex) {
			em.getTransaction().rollback();
			throw new DAOException("Erro ao remover Caminhao do banco de dados: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}

		return caminhao;
	}

	private boolean CaminhaoIsValid(Caminhao caminhao) {
		try {
			if ((caminhao.getPlaca().isEmpty()))
				return false;
		} catch (NullPointerException ex) {
			throw new DAOException("Caminhao com dados incompletos.", ErrorCode.BAD_REQUEST.getCode());
		}

		return true;
	}

	public List<Caminhao> getByPagination(int firstResult, int maxResults) {
		List<Caminhao> Caminhao;
		EntityManager em = JPAUtil.getEntityManager();

		try {
			Caminhao = em.createQuery("select p from Caminhao p", Caminhao.class).setFirstResult(firstResult - 1)
					.setMaxResults(maxResults).getResultList();
		} catch (RuntimeException ex) {
			throw new DAOException("Erro ao buscar Caminhao no banco de dados: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}

		if (Caminhao.isEmpty()) {
			throw new DAOException("P�gina com Caminhao vazia.", ErrorCode.NOT_FOUND.getCode());
		}

		return Caminhao;
	}

	public List<Caminhao> getByName(String placa) {
		EntityManager em = JPAUtil.getEntityManager();
		List<Caminhao> caminhao = null;

		try {
			caminhao = em.createQuery("select p from Caminhao p where p.placa like :placa", Caminhao.class)
					.setParameter("placa", "%" + placa + "%").getResultList();
		} catch (RuntimeException ex) {
			throw new DAOException("Erro ao buscar Caminhao por nome no banco de dados: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}

		if (caminhao.isEmpty()) {
			throw new DAOException("A consulta n�o encontrou Caminhao.", ErrorCode.NOT_FOUND.getCode());
		}

		return caminhao;
	}
}