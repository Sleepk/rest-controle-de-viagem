package br.com.devmedia.webservice.model.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.devmedia.webservice.exceptions.DAOException;
import br.com.devmedia.webservice.exceptions.ErrorCode;
import br.com.devmedia.webservice.model.domain.Motorista;

public class MotoristaDAO {

	/**
	 * Faz a busca de todos os motoristas cadastrados.
	 * 
	 * @return retorna os motoristas buscados
	 */
	public List<Motorista> getAll() {
		EntityManager em = JPAUtil.getEntityManager();
		List<Motorista> motoristas = null;

		try {
			motoristas = em.createQuery("select p from Motorista p", Motorista.class).getResultList();

			motoristas.toString();
		} catch (RuntimeException ex) {
			throw new DAOException("Erro ao recuperar todos os Motoristas do banco: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}

		return motoristas;
	}

	/**
	 * Faz a busca do motorista por ID
	 * 
	 * @param id
	 * @return todos motoristas
	 */
	public Motorista getById(long id) {
		EntityManager em = JPAUtil.getEntityManager();
		Motorista motorista = null;

		if (id <= 0) {
			throw new DAOException("O id precisa ser maior do que 0.", ErrorCode.BAD_REQUEST.getCode());
		}

		try {
			motorista = em.find(Motorista.class, id);
		} catch (RuntimeException ex) {
			throw new DAOException("Erro ao buscar Motorista por id no banco de dados: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}

		if (motorista == null) {
			throw new DAOException("Motorista de id " + id + " n�o existe.", ErrorCode.NOT_FOUND.getCode());
		}

		return motorista;
	}

	/**
	 * Metodo para salvar o motorista.
	 * 
	 * @param motorista
	 * @return motorista salvo
	 */
	public Motorista save(Motorista motorista) {
		EntityManager em = JPAUtil.getEntityManager();

		if (!MotoristaIsValid(motorista)) {
			throw new DAOException("Motorista com dados incompletos.", ErrorCode.BAD_REQUEST.getCode());
		}

		try {
			em.getTransaction().begin();
			em.persist(motorista);
			em.getTransaction().commit();
		} catch (RuntimeException ex) {
			em.getTransaction().rollback();
			throw new DAOException("Erro ao salvar Motorista no banco de dados: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}
		return motorista;
	}

	/**
	 * Metodo para editar o motorista, passando os dados do motorista por parametro.
	 * Onde � recebido os dados do {@link Motorista}
	 * 
	 * @param motorista
	 * 
	 * @throws ErrorCode.BAD_REQUEST
	 *             caso o id seja menor que 0, sistema retornar um status 400
	 * @throws ErrorCode.BAD_REQUEST
	 *             caso o motorista passado esteja com os dados incompleto, o
	 *             sistema informa um erro.
	 * @throws ErrorCode.NOT_FOUND
	 *             caso o motorista nao exista, o sistema retorna uma mensagem de
	 *             erro informando que o motorista nao existe.
	 * @throws ErrorCode.SERVER_ERROR
	 *             caso haja algum erro ao salvar o motorista no banco de dados �
	 *             estourado uma excessao de Server Error
	 * @return Retorna um motorista gerenciado
	 */
	public Motorista update(Motorista motorista) {
		EntityManager em = JPAUtil.getEntityManager();
		Motorista motoristaManaged = null;

		if (motorista.getId() <= 0) {
			throw new DAOException("O id precisa ser maior do que 0.", ErrorCode.BAD_REQUEST.getCode());
		}
		if (!MotoristaIsValid(motorista)) {
			throw new DAOException("Motorista com dados incompletos.", ErrorCode.BAD_REQUEST.getCode());
		}

		try {
			em.getTransaction().begin();
			motoristaManaged = em.find(Motorista.class, motorista.getId());
			motoristaManaged.setNome(motorista.getNome());
			motoristaManaged.setCelular(motorista.getCelular());
			motoristaManaged.setCnh(motorista.getCnh());
			motoristaManaged.setCpf(motorista.getCpf());
			motoristaManaged.setDataDeNascimento(motorista.getDataDeNascimento());
			motoristaManaged.setNome(motorista.getNome());
			motoristaManaged.setOrgaoExpedidor(motorista.getOrgaoExpedidor());
			motoristaManaged.setRg(motorista.getRg());
			motoristaManaged.setSexo(motorista.getSexo());
			motoristaManaged.setStatus(motorista.getStatus());
			motoristaManaged.setTelefone(motorista.getTelefone());

			em.getTransaction().commit();
		} catch (NullPointerException ex) {
			em.getTransaction().rollback();
			throw new DAOException("Motorista informado para atualiza��o n�o existe: " + ex.getMessage(),
					ErrorCode.NOT_FOUND.getCode());
		} catch (RuntimeException ex) {
			em.getTransaction().rollback();
			throw new DAOException("Erro ao atualizar Motorista no banco de dados: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}
		return motoristaManaged;
	}

	/**
	 * Metodo para deletar um motorista, passando o id por parametro.
	 * 
	 * Caso seja lan�ado alguma excessao sera lan�ado do tipo {@link ErrorCode}
	 * 
	 * @throws ErrorCode.BAD_REQUEST
	 *             Excessao lan�ada Caso o id for menor do que 0, o sistema retorna
	 *             um erro ao usuario.
	 * 
	 * @throws ErrorCode.NOT_FOUND
	 *             Excessao lan�ada quando o item informado nao existe, sistema
	 *             retorna um codigo de erro do tipo Not found
	 * 
	 * @throws ErrorCode.SERVER_ERROR
	 *             Excessao lan�ada quando � feito a tentativa de remover o item,
	 *             mas ela nao ocoorre. Sendo assim o banco faz um rollback.
	 * 
	 * @param Retorna
	 *            o id do motorista
	 * @return Status 204 Not Content
	 */
	public Motorista delete(Long id) {
		EntityManager em = JPAUtil.getEntityManager();
		Motorista motorista = null;

		if (id <= 0) {
			throw new DAOException("O id precisa ser maior do que 0.", ErrorCode.BAD_REQUEST.getCode());
		}

		try {
			em.getTransaction().begin();
			motorista = em.find(Motorista.class, id);
			em.remove(motorista);
			em.getTransaction().commit();
		} catch (IllegalArgumentException ex) {
			em.getTransaction().rollback();
			throw new DAOException("Motorista informado para remo��o n�o existe: " + ex.getMessage(),
					ErrorCode.NOT_FOUND.getCode());
		} catch (RuntimeException ex) {
			em.getTransaction().rollback();
			throw new DAOException("Erro ao remover Motorista do banco de dados: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}

		return motorista;
	}

	/**
	 * Procedimento para fazer as validacoes caso algum item deste esteja inconpleto
	 * o sistema lan�ara uma excessao.
	 * 
	 * @throws ErrorCode.BAD_REQUEST
	 *             Excessao do motorista incompleto.
	 * @param motorista
	 * @return
	 */
	private boolean MotoristaIsValid(Motorista motorista) {
		try {
			if ((motorista.getNome().isEmpty() || motorista.getNome().length() < 2)
					|| (motorista.getCpf().isEmpty()
							|| motorista.getCpf().length() < 0 && motorista.getCpf().length() <= 11)
					|| (motorista.getCnh().isEmpty()))
				return false;
		} catch (NullPointerException ex) {
			throw new DAOException("Motorista com dados incompletos.", ErrorCode.BAD_REQUEST.getCode());
		}

		return true;
	}

	/**
	 * Funcao de pagina��o, onde faz o retorno de um minimo de itens a ser exibido e
	 * um valor maximo.
	 * 
	 * @param firstResult
	 * @param maxResults
	 * @return
	 */
	public List<Motorista> getByPagination(int firstResult, int maxResults) {
		List<Motorista> Motoristas;
		EntityManager em = JPAUtil.getEntityManager();

		try {
			Motoristas = em.createQuery("select p from Motorista p", Motorista.class).setFirstResult(firstResult - 1)
					.setMaxResults(maxResults).getResultList();
		} catch (RuntimeException ex) {
			throw new DAOException("Erro ao buscar Motoristas no banco de dados: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}

		if (Motoristas.isEmpty()) {
			throw new DAOException("P�gina com Motoristas vazia.", ErrorCode.NOT_FOUND.getCode());
		}

		return Motoristas;
	}

	public List<Motorista> getByName(String name) {
		EntityManager em = JPAUtil.getEntityManager();
		List<Motorista> motorista = null;

		try {
			motorista = em.createQuery("select p from Motorista p where p.nome like :name", Motorista.class)
					.setParameter("name", "%" + name + "%").getResultList();
		} catch (RuntimeException ex) {
			throw new DAOException("Erro ao buscar Motoristas por nome no banco de dados: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}

		if (motorista.isEmpty()) {
			throw new DAOException("A consulta n�o encontrou Motoristas.", ErrorCode.NOT_FOUND.getCode());
		}

		return motorista;
	}
}