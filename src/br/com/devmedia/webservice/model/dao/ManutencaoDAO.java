package br.com.devmedia.webservice.model.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.devmedia.webservice.exceptions.DAOException;
import br.com.devmedia.webservice.exceptions.ErrorCode;
import br.com.devmedia.webservice.model.domain.Caminhao;
import br.com.devmedia.webservice.model.domain.Manutencao;

public class ManutencaoDAO {

	/**
	 * Retorna todas as manutencoes, informando o ID do caminhao.
	 * 
	 * @param id
	 * @return
	 */
	public List<Manutencao> getAll(long id) {
		EntityManager em = JPAUtil.getEntityManager();
		List<Manutencao> manutencao;

		try {
			manutencao = em.createQuery("select p from Manutencao p where p.caminhao.id = :pid", Manutencao.class)
					.setParameter("pid", id).getResultList();
		} catch (RuntimeException ex) {
			throw new DAOException("Erro ao recuperar todos os Manutencoes do banco: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}

		return manutencao;
	}

	/**
	 * Faz a busca da manutencao por ID.
	 * 
	 * @param id
	 * @return
	 */
	public Manutencao getById(long id) {
		EntityManager em = JPAUtil.getEntityManager();
		Manutencao manutencao;

		if (id <= 0) {
			throw new DAOException("O id precisa ser maior do que 0.", ErrorCode.BAD_REQUEST.getCode());
		}

		try {
			manutencao = em.find(Manutencao.class, id);
		} catch (RuntimeException ex) {
			throw new DAOException("Erro ao buscar Manutencao por id no banco de dados: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}

		if (manutencao == null) {
			throw new DAOException("Manutencao de id " + id + " n�o existe.", ErrorCode.NOT_FOUND.getCode());
		}

		return manutencao;
	}

	/**
	 * Salva a manutencao, recebendo por ID do caminhao. � feito a busca do iD e
	 * inseri a manutencao.
	 * 
	 * @param id
	 * @param manutencao
	 * @return
	 */
	public Manutencao save(long id, Manutencao manutencao) {
		EntityManager em = JPAUtil.getEntityManager();
		Caminhao caminhao;

		if (!ManutencaoIsValid(manutencao)) {
			throw new DAOException("Manutencao com dados incompletos.", ErrorCode.BAD_REQUEST.getCode());
		}

		try {

			em.getTransaction().begin();
			caminhao = em.find(Caminhao.class, id);
			caminhao.getManutencao().add(manutencao);
			manutencao.setCaminhao(caminhao);
			em.persist(manutencao);
			em.getTransaction().commit();

		} catch (RuntimeException ex) {
			em.getTransaction().rollback();
			throw new DAOException("Erro ao salvar Manutencao no banco de dados: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}
		return manutencao;
	}

	/**
	 * Atualiza a manutencao com id pelo caminhao.
	 * 
	 * @param id
	 * @param manutencao
	 * @return
	 */
	public Manutencao update(long id, Manutencao manutencao) {
		EntityManager em = JPAUtil.getEntityManager();
		Manutencao manutencaoManaged = null;

		if (manutencao.getId() <= 0) {
			throw new DAOException("O id precisa ser maior do que 0.", ErrorCode.BAD_REQUEST.getCode());
		}
		if (!ManutencaoIsValid(manutencao)) {
			throw new DAOException("Manutencao com dados incompletos.", ErrorCode.BAD_REQUEST.getCode());
		}

		try {
			em.getTransaction().begin();
			manutencaoManaged = em.find(Manutencao.class, manutencao.getId());
			manutencaoManaged.setDataEntrada(manutencao.getDataEntrada());
			manutencaoManaged.setDataSaida(manutencao.getDataSaida());
			manutencaoManaged.setServico(manutencao.getServico());
			manutencaoManaged.setCaminhao(manutencao.getCaminhao());

			if (manutencaoManaged.getCaminhao().getId() != id) {
				Caminhao caminhao = em.find(Caminhao.class, id);
				manutencaoManaged.setCaminhao(caminhao);
				caminhao.getManutencao().add(manutencaoManaged);
			}

			em.getTransaction().commit();
		} catch (NullPointerException ex) {
			em.getTransaction().rollback();
			throw new DAOException("Manutencao informado para atualiza��o n�o existe: " + ex.getMessage(),
					ErrorCode.NOT_FOUND.getCode());
		} catch (RuntimeException ex) {
			em.getTransaction().rollback();
			throw new DAOException("Erro ao atualizar Manutencao no banco de dados: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}
		return manutencaoManaged;
	}

	public Manutencao delete(Long id) {
		EntityManager em = JPAUtil.getEntityManager();
		Manutencao motorista = null;

		if (id <= 0) {
			throw new DAOException("O id precisa ser maior do que 0.", ErrorCode.BAD_REQUEST.getCode());
		}

		try {
			em.getTransaction().begin();
			motorista = em.find(Manutencao.class, id);
			em.remove(motorista);
			em.getTransaction().commit();
		} catch (IllegalArgumentException ex) {
			em.getTransaction().rollback();
			throw new DAOException("Manutencao informado para remo��o n�o existe: " + ex.getMessage(),
					ErrorCode.NOT_FOUND.getCode());
		} catch (RuntimeException ex) {
			em.getTransaction().rollback();
			throw new DAOException("Erro ao remover Manutencao do banco de dados: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}

		return motorista;
	}

	private boolean ManutencaoIsValid(Manutencao motorista) {
		try {
			if ((motorista.getServico().isEmpty()))
				return false;
		} catch (NullPointerException ex) {
			throw new DAOException("Manutencao com dados incompletos.", ErrorCode.BAD_REQUEST.getCode());
		}

		return true;
	}

	public List<Manutencao> getByPagination(long id, int firstResult, int maxResults) {
		List<Manutencao> Manutencoes;
		EntityManager em = JPAUtil.getEntityManager();

		try {
			Manutencoes = em.createQuery("select p from Manutencao p", Manutencao.class).setFirstResult(firstResult - 1)
					.setMaxResults(maxResults).getResultList();
		} catch (RuntimeException ex) {
			throw new DAOException("Erro ao buscar Manutencoes no banco de dados: " + ex.getMessage(),
					ErrorCode.SERVER_ERROR.getCode());
		} finally {
			em.close();
		}

		if (Manutencoes.isEmpty()) {
			throw new DAOException("P�gina com Manutencoes vazia.", ErrorCode.NOT_FOUND.getCode());
		}

		return Manutencoes;
	}

}