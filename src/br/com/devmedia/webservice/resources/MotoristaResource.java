package br.com.devmedia.webservice.resources;

import java.util.List;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.devmedia.webservice.model.domain.Motorista;
import br.com.devmedia.webservice.resources.beans.FilterBean;
import br.com.devmedia.webservice.service.MotoristaService;

@Path("/motoristas")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MotoristaResource {

	private MotoristaService service = new MotoristaService();

	@GET
	public List<Motorista> getMotoristas(@BeanParam FilterBean filter) {
		if ((filter.getOffset() >= 0) && (filter.getLimit() > 0)) {
			return service.getMotoristasByPagination(filter.getOffset(), filter.getLimit());
		}
		if (filter.getName() != null) {
			return service.getMotoristaByName(filter.getName());
		}

		return service.getMotoristas();
	}

	@GET
	@Path("{produtoId}")
	public Motorista getMotorista(@PathParam("produtoId") long id) {
		return service.getMotorista(id);
	}

	@POST
	public Response save(Motorista produto) {

		produto = service.saveMotorista(produto);
		return Response.status(Status.CREATED).entity(produto).build();
	}

	@PUT
	@Path("{produtoId}")
	public void update(@PathParam("produtoId") long id, Motorista produto) {
		produto.setId(id);
		service.updateMotorista(produto);
	}

	@DELETE
	@Path("{produtoId}")
	public void delete(@PathParam("produtoId") long id) {
		service.deleteMotorista(id);
	}
}
