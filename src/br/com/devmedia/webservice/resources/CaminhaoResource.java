package br.com.devmedia.webservice.resources;

import java.util.List;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.devmedia.webservice.model.domain.Caminhao;
import br.com.devmedia.webservice.resources.beans.FilterBean;
import br.com.devmedia.webservice.service.CaminhaoService;

@Path("/caminhoes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CaminhaoResource {

	private CaminhaoService service = new CaminhaoService();

	@GET
	public List<Caminhao> getCaminhaos(@BeanParam FilterBean filter) {
		if ((filter.getOffset() >= 0) && (filter.getLimit() > 0)) {
			return service.getCaminhaosByPagination(filter.getOffset(), filter.getLimit());
		}
		if (filter.getName() != null) {
			return service.getCaminhaoByName(filter.getName());
		}

		return service.getCaminhoes();
	}

	@GET
	@Path("{caminhaoId}")
	public Caminhao getCaminhao(@PathParam("caminhaoId") long id) {
		return service.getCaminhao(id);
	}

	@POST
	public Response save(Caminhao produto) {
		produto = service.saveCaminhao(produto);
		return Response.status(Status.CREATED).entity(produto).build();
	}

	@PUT
	@Path("{caminhaoId}")
	public void update(@PathParam("caminhaoId") long id, Caminhao produto) {
		produto.setId(id);
		service.updateCaminhao(produto);
	}

	@DELETE
	@Path("{caminhaoId}")
	public void delete(@PathParam("caminhaoId") long id) {
		service.deleteCaminhao(id);
	}

	@Path("{caminhaoId}/manutencao")
	public ManutencaoResource getProdutoResource() {
		return new ManutencaoResource();
	}
}
