package br.com.devmedia.webservice.resources;

import java.util.List;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.devmedia.webservice.model.domain.Manutencao;
import br.com.devmedia.webservice.resources.beans.FilterBean;
import br.com.devmedia.webservice.service.ManutencaoService;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ManutencaoResource {

	private ManutencaoService service = new ManutencaoService();

	@GET
	public List<Manutencao> getManutencoes(@PathParam("manutencaoId") long id, @BeanParam FilterBean filter) {
		if ((filter.getOffset() >= 0) && (filter.getLimit() > 0)) {
			return service.getManutencaosByPagination(id, filter.getOffset(), filter.getLimit());
		}

		return service.getManutencoes(id);
	}

	@GET
	@Path("{manutencaoId}")
	public Manutencao getManutencao(@PathParam("manutencaoId") long id) {
		return service.getManutencao(id);
	}

	@POST
	public Response save(@PathParam("manutencaoId") long id, Manutencao produto) {
		produto = service.saveManutencao(id, produto);
		return Response.status(Status.CREATED).entity(produto).build();
	}

	@PUT
	@Path("{manutencaoId}")
	public void update(@PathParam("manutencaoId") long id, Manutencao manutencao) {
		manutencao.setId(id);
		service.updateManutencao(id, manutencao);
	}

	@DELETE
	@Path("{manutencaoId}")
	public void delete(@PathParam("manutencaoId") long id) {
		service.deleteManutencao(id);
	}

}
