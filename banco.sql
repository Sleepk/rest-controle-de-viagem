CREATE DATABASE  IF NOT EXISTS `banco` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `banco`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: banco
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `abastecimento`
--

DROP TABLE IF EXISTS `abastecimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abastecimento` (
  `idAbastecimento` bigint(20) NOT NULL AUTO_INCREMENT,
  `abast_nomePosto` varchar(45) NOT NULL,
  `abast_qdtOleo` decimal(5,2) NOT NULL,
  `abast_valorReais` decimal(10,2) NOT NULL,
  `abast_dataAbastecimento` date NOT NULL,
  `asbast_viag_idViagem` bigint(20) NOT NULL,
  PRIMARY KEY (`idAbastecimento`,`asbast_viag_idViagem`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abastecimento`
--

LOCK TABLES `abastecimento` WRITE;
/*!40000 ALTER TABLE `abastecimento` DISABLE KEYS */;
/*!40000 ALTER TABLE `abastecimento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `caminhao`
--

DROP TABLE IF EXISTS `caminhao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caminhao` (
  `idCaminhao` bigint(20) NOT NULL AUTO_INCREMENT,
  `cam_placa` varchar(14) NOT NULL,
  `cam_chassi` varchar(17) NOT NULL,
  `cam_renavam` int(11) NOT NULL,
  `cam_anoFabricacao` year(4) DEFAULT NULL,
  `cam_Modelo` varchar(20) DEFAULT NULL,
  `cam_Marca` varchar(20) DEFAULT NULL,
  `cam_odometro` decimal(10,3) NOT NULL,
  `cam_ConsumoMedioLKM` decimal(10,2) DEFAULT NULL,
  `cam_status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`idCaminhao`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caminhao`
--

LOCK TABLES `caminhao` WRITE;
/*!40000 ALTER TABLE `caminhao` DISABLE KEYS */;
INSERT INTO `caminhao` VALUES (1,'WCE1414','J2D0I8D',2147483647,2017,'MD093','Chevrolet',8.000,6.00,'DISPONIVEL '),(2,'WTH9003','L2Y1S5S',2147483647,2018,'EB730','FAW',8.000,4.00,'DISPONIVEL '),(3,'TVW8363','X9B0C9J',2147483647,2018,'BD091','Infiniti',7.000,10.00,' INDISPONIVEL'),(4,'GLD4982','X8Q1S7W',2147483647,2018,'BW454','Isuzu',6.000,7.00,' INDISPONIVEL'),(5,'IUQ0053','R8Q6F5B',1648854,2017,'UF092','Acura',10.000,5.00,'DISPONIVEL '),(6,'JGQ1951','P7A2E5W',2147483647,2018,'EW455','Mazda',5.000,10.00,' INDISPONIVEL'),(7,'POX1549','G9G9R0B',2147483647,2017,'AL975','Lincoln',1.000,6.00,' INDISPONIVEL'),(8,'EIF0245','A7T8J9F',2147483647,2018,'LC682','Kenworth',5.000,1.00,' INDISPONIVEL'),(9,'HCT1556','Z9J1A6S',100366917,2018,'AT283','Infiniti',3.000,1.00,' INDISPONIVEL'),(10,'IKY7987','O7D2I4D',2147483647,2017,'YD356','Isuzu',10.000,1.00,' INDISPONIVEL'),(11,'OGL4307','O1X7M0H',2147483647,2017,'HH607','Citroën',1.000,4.00,' INDISPONIVEL'),(12,'WBQ9840','G0E8Z2K',2147483647,2017,'LJ138','Mahindra',2.000,9.00,' INDISPONIVEL'),(13,'JQF0818','M8E4A6T',2147483647,2017,'IT290','Chevrolet',6.000,3.00,'DISPONIVEL '),(14,'NOI4915','S4U3R0I',2147483647,2018,'PC430','Ford',7.000,4.00,'DISPONIVEL '),(15,'OBT4310','T6N3K0P',2147483647,2018,'BU518','Peugeot',3.000,2.00,' INDISPONIVEL'),(16,'BCE0496','V8G9G4X',2147483647,2017,'FU728','GMC',10.000,9.00,'DISPONIVEL '),(17,'DCZ4361','I3J1X4Z',2147483647,2017,'ME086','Dodge',10.000,4.00,' INDISPONIVEL'),(18,'KXD8551','D0Z0J9G',2147483647,2017,'LD091','Lexus',7.000,3.00,' INDISPONIVEL'),(19,'RXK5283','E2M1S1Y',2147483647,2017,'XJ513','Lexus',9.000,3.00,' INDISPONIVEL'),(20,'UBF4418','F0D6W6Z',2147483647,2017,'FF737','Mitsubishi Motors',8.000,5.00,' INDISPONIVEL');
/*!40000 ALTER TABLE `caminhao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carga`
--

DROP TABLE IF EXISTS `carga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carga` (
  `idCarga` bigint(20) NOT NULL AUTO_INCREMENT,
  `carg_Tipo` varchar(30) NOT NULL,
  `carg_quantidade` decimal(10,2) NOT NULL,
  `carg_unidade` varchar(30) DEFAULT NULL,
  `id` bigint(20) NOT NULL,
  `quantidade` double DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `unidade` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idCarga`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carga`
--

LOCK TABLES `carga` WRITE;
/*!40000 ALTER TABLE `carga` DISABLE KEYS */;
INSERT INTO `carga` VALUES (1,'1',1.00,'1',0,NULL,NULL,NULL),(11,'carga ',1.00,' kg',0,NULL,NULL,NULL),(12,' grao',4.00,' kg',0,NULL,NULL,NULL),(13,'carga ',4.00,' kg',0,NULL,NULL,NULL),(14,' grao',5.00,'litro ',0,NULL,NULL,NULL),(15,'carga ',6.00,' kg',0,NULL,NULL,NULL),(16,'carga ',1.00,'litro ',0,NULL,NULL,NULL),(17,' grao',1.00,' kg',0,NULL,NULL,NULL),(18,'carga ',7.00,'litro ',0,NULL,NULL,NULL),(19,' grao',9.00,'litro ',0,NULL,NULL,NULL),(20,'carga ',6.00,'litro ',0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `carga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gastoextra`
--

DROP TABLE IF EXISTS `gastoextra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gastoextra` (
  `idGastoExtra` bigint(20) NOT NULL AUTO_INCREMENT,
  `gast_RazaoSocial` varchar(45) NOT NULL,
  `gast_valor` decimal(10,2) NOT NULL,
  `gast_data` date NOT NULL,
  `gast_viag_idViagem` int(11) NOT NULL,
  PRIMARY KEY (`idGastoExtra`,`gast_viag_idViagem`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gastoextra`
--

LOCK TABLES `gastoextra` WRITE;
/*!40000 ALTER TABLE `gastoextra` DISABLE KEYS */;
/*!40000 ALTER TABLE `gastoextra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manutencao`
--

DROP TABLE IF EXISTS `manutencao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manutencao` (
  `idManutencao` bigint(20) NOT NULL AUTO_INCREMENT,
  `man_dataEntrada` date DEFAULT NULL,
  `man_dataSaida` date DEFAULT NULL,
  `caminhao` bigint(20) NOT NULL,
  PRIMARY KEY (`idManutencao`,`caminhao`),
  KEY `fk_man_caminhao_idx` (`caminhao`),
  CONSTRAINT `fk_man_caminhao` FOREIGN KEY (`caminhao`) REFERENCES `caminhao` (`idCaminhao`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manutencao`
--

LOCK TABLES `manutencao` WRITE;
/*!40000 ALTER TABLE `manutencao` DISABLE KEYS */;
INSERT INTO `manutencao` VALUES (1,'2017-11-20','2017-11-22',1),(2,'2017-05-09','2017-11-13',19);
/*!40000 ALTER TABLE `manutencao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `motorista`
--

DROP TABLE IF EXISTS `motorista`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `motorista` (
  `idMotorista` bigint(20) NOT NULL AUTO_INCREMENT,
  `mot_nome` varchar(45) NOT NULL,
  `mot_cpf` varchar(11) NOT NULL,
  `mot_rg` varchar(30) NOT NULL,
  `mot_orgaoExpedidor` varchar(30) DEFAULT NULL,
  `mot_dataNasc` date DEFAULT NULL,
  `mot_cnh` int(11) NOT NULL,
  `mot_sexo` char(1) DEFAULT 'M',
  `mot_telefone` varchar(13) DEFAULT NULL,
  `mot_celular` varchar(13) DEFAULT NULL,
  `mot_status` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`idMotorista`),
  UNIQUE KEY `mot_cpf_UNIQUE` (`mot_cpf`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `motorista`
--

LOCK TABLES `motorista` WRITE;
/*!40000 ALTER TABLE `motorista` DISABLE KEYS */;
INSERT INTO `motorista` VALUES (21,'Orson','4132104191','3605160360',NULL,'1979-12-06',953388936,'M',NULL,'74-66699-5786',NULL),(22,'Quon','6365993752','6368713408',NULL,'1988-03-13',661874784,'M',NULL,'81-96537-3850',NULL),(23,'Mollie','9075583030','5227110323',NULL,'1999-04-08',501398404,'M',NULL,'12-66282-1033',NULL),(24,'Ila','4684281784','0579111966',NULL,'2016-05-11',148276916,'M',NULL,'10-81248-6747',NULL),(25,'Selma','7713581594','6435396358',NULL,'2010-06-28',338862732,'M',NULL,'61-65521-7543',NULL),(26,'Jada','1963364396','6636974071',NULL,'1980-12-02',285616118,'M',NULL,'48-41198-1463',NULL),(27,'Lysandra','2225881139','3608658142',NULL,'1976-01-12',305323463,'M',NULL,'77-66054-6087',NULL),(28,'Caryn','9347815653','0923491278',NULL,'1985-09-10',313431796,'M',NULL,'45-29082-5523',NULL),(29,'Drake','1250698608','8356648080',NULL,'2002-01-23',369029530,'M',NULL,'40-86311-2349',NULL),(30,'Orlando','5482440447','2755371448',NULL,'1974-10-12',690422029,'M',NULL,'97-87538-6142',NULL),(31,'Colton Gutierrez','6902283876','2999511096',NULL,'1997-03-14',701472221,'M','37-3684-1639','64-13825-1808',NULL),(32,'Rooney Y. Gentry','5842632673','8643414921',NULL,'1997-07-07',191155811,'M','15-1101-8177','90-30603-9118',NULL),(33,'Fulton Roberson','4547271643','5503496416',NULL,'1974-04-19',973071034,'M','31-2372-5875','12-92787-4944',NULL),(34,'Minerva N. Franks','2581315830','7994795575',NULL,'1989-04-15',199804056,'M','72-5599-9482','12-90904-9032',NULL),(35,'Dale Barron','2385737263','8407900757',NULL,'1995-05-18',767515479,'M','61-5484-1684','77-34045-4963',NULL),(36,'Phyllis Y. Reid','5875484394','2116957000',NULL,'1985-08-06',877971357,'M','46-5321-2762','75-28491-7387',NULL),(37,'Lester R. Zimmerman','5046450211','5171885786',NULL,'2008-04-16',341433318,'M','43-8764-7298','40-82969-5758',NULL),(38,'Lareina U. Skinner','7693230393','9368609990',NULL,'1988-02-29',390737761,'M','21-6002-6403','25-08030-7810',NULL),(39,'Todd Howell','0894194813','5772395924',NULL,'1998-12-07',892982598,'M','90-2204-3397','74-00891-0310',NULL),(40,'Cullen C. Estrada','4764978735','0656685016',NULL,'2017-12-06',358684230,'M','54-7307-8968','89-80793-2122',NULL);
/*!40000 ALTER TABLE `motorista` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedagio`
--

DROP TABLE IF EXISTS `pedagio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedagio` (
  `idpedagio` bigint(20) NOT NULL AUTO_INCREMENT,
  `ped_data` date NOT NULL,
  `ped_praca` varchar(30) NOT NULL,
  `ped_valor` decimal(10,2) NOT NULL,
  `ped_viag_idViagem` int(11) NOT NULL,
  PRIMARY KEY (`idpedagio`,`ped_viag_idViagem`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='		';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedagio`
--

LOCK TABLES `pedagio` WRITE;
/*!40000 ALTER TABLE `pedagio` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedagio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `viagem`
--

DROP TABLE IF EXISTS `viagem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `viagem` (
  `idViagem` bigint(20) NOT NULL AUTO_INCREMENT,
  `viag_dataInicio` date NOT NULL,
  `viag_dataFim` date DEFAULT NULL,
  `viag_odometroInicial` decimal(10,2) DEFAULT NULL,
  `viag_odometroFinal` decimal(10,2) DEFAULT NULL,
  `viag_totalKMPercorrido` decimal(10,2) DEFAULT NULL,
  `viag_diariaDoMotorista` decimal(7,2) NOT NULL,
  `viag_status` varchar(15) DEFAULT NULL,
  `viag_carroceria` varchar(20) DEFAULT NULL,
  `viag_cam_idCaminhao` bigint(20) NOT NULL,
  `viag_mot_idMotorista` bigint(20) NOT NULL,
  `viag_carg_idCarga` bigint(20) NOT NULL,
  `viag_pedagio_idPedagio` bigint(20) NOT NULL,
  PRIMARY KEY (`idViagem`,`viag_cam_idCaminhao`,`viag_mot_idMotorista`,`viag_carg_idCarga`,`viag_pedagio_idPedagio`),
  KEY `fk_viagem_motorista_idx` (`viag_mot_idMotorista`),
  KEY `fk_viagem_caminhao_idx` (`viag_cam_idCaminhao`),
  KEY `fk_viagem_carga_idx` (`viag_carg_idCarga`),
  KEY `fk_viagem_pedagio_idx` (`viag_pedagio_idPedagio`),
  CONSTRAINT `fk_viagem_caminhao` FOREIGN KEY (`viag_cam_idCaminhao`) REFERENCES `caminhao` (`idCaminhao`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_viagem_carga` FOREIGN KEY (`viag_carg_idCarga`) REFERENCES `carga` (`idCarga`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_viagem_motorista` FOREIGN KEY (`viag_mot_idMotorista`) REFERENCES `motorista` (`idMotorista`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `viagem`
--

LOCK TABLES `viagem` WRITE;
/*!40000 ALTER TABLE `viagem` DISABLE KEYS */;
/*!40000 ALTER TABLE `viagem` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-21 18:08:30
