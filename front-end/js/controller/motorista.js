
function cadastrarMotorista() {
	var dataDeNascimento = $('input[id=dataDeNascimento]').val();
	var sexo = $('input[name=sexo]').val();
	var nome = $('input[id=nome]').val();
	var telefone = $('input[id=telefone]').val();
	var celular = $('input[id=celular]').val();
	var rg = $('input[id=rg]').val();
	var orgaoExpedidor = $('input[id=orgaoExpedidor]').val();
	var cpf = $('input[id=cpf]').val();
	var cnh = $('input[id=cnh]').val();
	var status = $('input[id=status]').val();

	var motorista = { 
		"nome" : nome,
		"cpf" : cpf,
		"rg" : rg,
		"orgaoExpedidor" : orgaoExpedidor,
		"dataDeNascimento": {
            "year": 1979,
            "month": 12,
            "day": 6
        },
		"cnh" : cnh,
		"sexo": sexo,
		"telefone" : telefone,
		"celular" : celular,
		"status" : "disponivel"
	};
	
	var request = new XMLHttpRequest();
	var url = "http://localhost:8080/webservice/webapi/motoristas";	

	request.open("POST", url, false);
	request.setRequestHeader("Content-type", "application/json");
	var data = JSON.stringify(motorista);
	request.send(data);
}

function editar() {

}

function remover(id) {
	var request = new XMLHttpRequest();
	var url = "http://localhost:8080/webservice/webapi/motoristas/" + id;	
	
	request.open("DELETE", url, false);
	request.setRequestHeader("Content-type", "application/json");
	request.send();
}

function listarTodos() {
	var request = new XMLHttpRequest();
	var url = "http://localhost:8080/webservice/webapi/motoristas";
	var place = document.getElementById('listagem');

	var tabela = "";
	tabela += "<table class='table'>";
	tabela += "<thead><tr>";
	tabela += "<th>Motorista</th>";
	tabela += "<th>Data de Nascimento</th>";
	tabela += "<th>Status</th>";
	tabela += "<th>Ações</th>";
	tabela += "</tr></thead>";

	tabela += "<tbody>";

	request.onreadystatechange = function () {
		if (request.readyState == 4 && request.status == 200) {
			var motorista = JSON.parse(request.response);

			for(var i = 0; i < motorista.length; i++) {
				tabela += "<tr>";
				tabela += "<th>"+ motorista.nome +"</th>";
				tabela += "<th>"+ motorista.dataDeNascimento +"</th>";
				tabela += "<th>"+ motorista.status +"</th>";
				tabela += "<th>"+ "<button class='btn btn-danger'> Excluir </button>" +"</th>";
				tabela += "</tr>";
			}
		}
	};			

	tabela += "</tbody>";
	tabela += "</table>";

	place.innerHTML = tabela;  
}