
function cadastrarCaminhao() {

	var placa = $('input[id=placa]').val();
	var chassi = $('input[id=chassi]').val();
	var renavam = $('input[id=renavam]').val();
	var anoFabricacao = $('input[id=anoFabricacao]').val();
	var anoModelo = $('input[id=anoModelo]').val();
	var modelo = $('input[id=modelo]').val();
	var odometro = $('input[id=odometro]').val();
	var consumoMedioDeCombustivel = $('input[id=consumoMedioDeCombustivel]').val();
	var status = $('input[id=status]').val();

	if (placa == '' || chassi == '' || renavam == '' ||  anoFabricacao == '' || anoModelo == '' || modelo == '' ||  odometro == '' || consumoMedioDeCombustivel == '' || status == '') {
		return false;
	}

	var caminhao = { 
		'placa': placa,
		'chassi': chassi,
		'renavam' : renavam,
		'anoFabricacao' : anoFabricacao,
		'anoModelo' : $anoModelo,
		'modelo' : modelo,
		'odometro' : odometro,
		'consumoMedioDeCombustivel' : consumoMedioDeCombustivel,
		'status' : status
	};

	var request = new XMLHttpRequest();
	var url = "http://localhost:8080/webservice/webapi/caminhoes";	

	request.open("POST", url, false);
	request.setRequestHeader("Content-type", "application/json");
	var data = JSON.stringify(caminhao);
	request.send(data);
}

function editar() {

}

function remover(id) {
	var request = new XMLHttpRequest();
	var url = "http://localhost:8080/webservice/webapi/caminhoes/" + id;	
	
	request.open("DELETE", url, true);
	request.setRequestHeader("Content-type", "application/json");
	request.send();
}